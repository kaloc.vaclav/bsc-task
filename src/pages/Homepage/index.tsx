import React, { useEffect, useContext } from "react";
import { TodosList } from "../../components/TodosList";
import AddTodo from "../../components/AddTodo";
import { TodosContext } from "../../context/TodoContext";

export const Homepage = () => {
  const todosContext = useContext(TodosContext);

  useEffect(() => {
    todosContext.fetchTodos();
  }, []); // eslint-disable-line

  return (
    <>
      <AddTodo />
      <TodosList />
    </>
  );
};
