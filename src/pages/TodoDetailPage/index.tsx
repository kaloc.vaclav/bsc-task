import React, { useState, useContext, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Card } from "antd";
import { useTranslation } from "react-i18next";
import { TodoDetail } from "../../components/TodoDetail";
import { ITodo } from "../../components/Todo/interaces";
import { TodosContext } from "../../context/TodoContext";

export const TodoDetailPage = () => {
  const { t } = useTranslation();
  const { id } = useParams();
  const [todo, setTodo] = useState<ITodo | null>(null);
  const todosContext = useContext(TodosContext);

  useEffect(() => {
    const fetchDetail = async () => {
      try {
        const result = await todosContext.fetchTodoDetail(Number(id));
        setTodo(result);
      } catch (err) {
        console.error(err);
      }
    };
    fetchDetail();
  }, [id, t]); // eslint-disable-line

  if (todosContext.loading) {
    return <Card loading={todosContext.loading}></Card>;
  }

  if (!todo) {
    return <Card>{t("incorrectId")}</Card>;
  }

  return <TodoDetail todo={todo} />;
};
