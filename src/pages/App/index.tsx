import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PageLayout from "../../components/layout/PageLayout";
import { Homepage } from "../Homepage";
import { PageNotFound } from "../PageNotFound";
import { TodosContextProvider } from "../../context/TodoContext";
import { TodoDetailPage } from "../TodoDetailPage";

const App = () => {
  return (
    <Router>
      <PageLayout>
        <TodosContextProvider>
          <Switch>
            <Route exact path="/">
              <Homepage />
            </Route>
            <Route path="/todos">
              <Homepage />
            </Route>
            <Route path="/todo/:id">
              <TodoDetailPage />
            </Route>
            <Route path="*">
              <PageNotFound />
            </Route>
          </Switch>
        </TodosContextProvider>
      </PageLayout>
    </Router>
  );
};

export default App;
