export interface ITodoProps {
  todo: ITodo;
}

export interface ITodo {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}
