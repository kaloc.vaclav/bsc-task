import React, { useContext } from "react";
import { Card, Typography, Button } from "antd";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { ITodoDetailProps } from "./interfaces";
import { TodosContext } from "../../context/TodoContext";

const { Paragraph } = Typography;

export const TodoDetail: React.FC<ITodoDetailProps> = ({ todo }) => {
  const { t } = useTranslation();
  const todosContext = useContext(TodosContext);
  const history = useHistory();

  const goToHomepage = () => history.push("/");

  const handleOnChange = async (text: string) => {
    await todosContext.handleTodoUpdate(todo.id, text);
    goToHomepage();
  };

  return (
    <Card style={{ marginTop: "30px" }}>
      <Paragraph editable={{ onChange: text => handleOnChange(text) }}>
        {todo.title}
      </Paragraph>
      <Button type="primary" onClick={goToHomepage}>
        {t("backHome")}
      </Button>
    </Card>
  );
};
