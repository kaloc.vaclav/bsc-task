import styled from "styled-components";
import Result from "antd/lib/result";

export const StyledResult = styled(Result)`
  .ant-result-title,
  .ant-result-subtitle {
    color: white;
  }
`;
