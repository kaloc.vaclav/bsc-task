import React from "react";
import Button from "antd/lib/button";
import { useTranslation } from "react-i18next";
import { StyledResult } from "./styled";
import { useLocation, useHistory } from "react-router-dom";

export const NotFound = () => {
  const { t } = useTranslation();
  const location = useLocation();
  const history = useHistory();

  return (
    <StyledResult
      status="404"
      title="404"
      subTitle={t("404NotFound", {
        location: location.pathname
      })}
      extra={
        <Button onClick={() => history.push("/")} type="primary">
          {t("backHome")}
        </Button>
      }
    />
  );
};
