import React, { useContext } from "react";
import Form from "antd/lib/form";
import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Input from "antd/lib/input";
import Typography from "antd/lib/typography";
import { useTranslation } from "react-i18next";
import { IAddTodoProps } from "./interace";
import { StyledButton } from "./styled";
import { TodosContext } from "../../context/TodoContext";

const { Text } = Typography;

const AddTodo: React.FC<IAddTodoProps> = ({ form }) => {
  const { getFieldDecorator } = form;
  const { t } = useTranslation();
  const todosContext = useContext(TodosContext);

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    form.validateFields((err, value) => {
      if (!err) {
        form.resetFields();
        todosContext.handleTodoCreate(value.text);
      }
    });
  };

  return (
    <Form onSubmit={e => handleSubmit(e)}>
      <Row gutter={20}>
        <Col xs={24} sm={24} md={17} lg={19} xl={20}>
          <Form.Item>
            {getFieldDecorator("text", {
              rules: [
                {
                  required: true,
                  message: t("required")
                }
              ]
            })(<Input placeholder={t("placeholder")} spellCheck={false} />)}
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={7} lg={5} xl={4}>
          <StyledButton
            type="primary"
            htmlType="submit"
            block
            loading={todosContext.loading}
          >
            <Text strong>{t("addButton")}</Text>
          </StyledButton>
        </Col>
      </Row>
    </Form>
  );
};

export default Form.create<IAddTodoProps>({ name: "AddTodo" })(AddTodo);
