import styled from "styled-components";
import Icon from "antd/lib/icon";
import { Layout } from "antd";
const { Header } = Layout;

export const StyledHeader = styled(Header)`
  width: 100%;
  min-height: 7vh;
  color: white;
`;

export const StyledIcon = styled(Icon)`
  margin: 0 10px;
`;

export const StyledLogo = styled.div`
  cursor: pointer;
`;
