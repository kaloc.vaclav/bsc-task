import React from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { Row, Col } from "antd";
import { CzechFlag } from "../../../static/svg/czech-republic.jsx";
import { UKFlag } from "../../../static/svg/united-kingdom.jsx";
import { StyledHeader, StyledIcon, StyledLogo } from "./styled";

export const Header: React.FC = () => {
  const { t, i18n } = useTranslation();
  const history = useHistory();
  return (
    <StyledHeader>
      <Row type="flex">
        <Col style={{ textAlign: "left" }} xs={18} md={20} lg={22}>
          <StyledLogo onClick={() => history.push("/")}>
            {t("welcome")}
          </StyledLogo>
        </Col>
        <Col xs={3} md={2} lg={1}>
          <StyledIcon
            onClick={() => i18n.changeLanguage("cs")}
            component={CzechFlag}
          />
        </Col>
        <Col xs={3} md={2} lg={1}>
          <StyledIcon
            onClick={() => i18n.changeLanguage("en")}
            component={UKFlag}
          />
        </Col>
      </Row>
    </StyledHeader>
  );
};
