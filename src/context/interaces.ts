import { ITodo } from "../components/Todo/interaces";

export interface TodoContext {
  items: ITodo[];
  loading: boolean;
  fetchTodos: () => void;
  fetchTodoDetail: (id: number) => Promise<ITodo | null>;
  handleTodoUpdate: (id: number, text: string) => Promise<void>;
  handleCheckboxToggle: (id: number) => Promise<void>;
  handleTodoDelete: (id: number) => Promise<void>;
  handleTodoCreate: (text: string) => Promise<void>;
}
